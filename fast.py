from typing import Optional
from fastapi import FastAPI, Query, Path,Form,File,UploadFile,HTTPException,Request
from pydantic import BaseModel,EmailStr
from fastapi.responses import JSONResponse

app = FastAPI()


@app.get('/')
async def wait():
    total = 0
    for i in range(0, 100000):
        total = total + i
    return {"total": total}


@app.get("/items/{item_id}")
def read_items_id(item_id: int, q: Optional[str] = None):
    return {"item_id": item_id, "q": q}

# This is for the request body like post api


class Item(BaseModel):
    first_name: str
    age: int
    middle_name: Optional[str] = None


@app.post('/datas/')
async def post_data(item: Item):
    return item

# QUERY PARAMETER VALIDATION


@app.get("/check_value")
async def notgreaterthan50(item: Optional[str] = Query(None, max_length=10)):
    dic_data = {'name': "suraj", 'college': "amrit"}
    if item:
        dic_data.update({"item": item})
    else:
        print("wrong")
    return dic_data

# PATH PARAMETER VALIDATION
#  I think ... means required


@app.post('/path_details/{id}')
async def path_detail(id: int = Path(..., title="fskldfjls slkfjlk"), q: Optional[str] = Query(None)):
    custom_data = {"id": id}
    if q:
        custom_data.update({'q': q})
    return custom_data
# Body multiple parameter means passing path query and body as optional


class body_data(BaseModel):
    name: str
    age: int
    city: Optional[str] = None


@app.put('/edit_data/{id}')
async def edit_datas(id: int = Path(..., title="jpt", ge=0, le=10),
                     q:Optional[str]=Query(None, max_length=10),
                     item: Optional[body_data] = None):
    datas = {'id': id}
    if q:
        datas.update({"q":q})
    if item:
        datas.update({'item':item})
    return datas

# extra schema

class Item3(BaseModel):
    name: str
    description: Optional[str] = None
    price: float
    tax: Optional[float] = None

    class Config:
        schema_extra = {
            "example": {
                "name": "Foo",
                "description": "A very nice Item",
                "price": 35.4,
                "tax": 3.2,
            }
        }


@app.put("/nepal/{item_id}")
async def update_item(item_id: int, item: Item3):
    results = {"item_id": item_id, "item": item}
    return results

# Response model is for response of data from output model 

class UserIn(BaseModel):
    username: str
    password: str
    email: EmailStr
    full_name: Optional[str] = None


class UserOut(BaseModel):
    username: str
    email: EmailStr
    nepal:int
    full_name: Optional[str] = None


@app.post("/user/", response_model=UserOut)
async def create_user(user: UserIn):
    return user


from typing import List

import databases
import sqlalchemy


# SQLAlchemy specific code, as with any other app
DATABASE_URL = "sqlite:///./test.db"

database = databases.Database(DATABASE_URL)

metadata = sqlalchemy.MetaData()
print("meta data is ++++++++++",metadata)
notes = sqlalchemy.Table(
    "notes",
    metadata,
    sqlalchemy.Column("id", sqlalchemy.Integer, primary_key=True),
    sqlalchemy.Column("text", sqlalchemy.String),
    sqlalchemy.Column("completed", sqlalchemy.Boolean),
)


engine = sqlalchemy.create_engine(
    DATABASE_URL, connect_args={"check_same_thread": False}
)

metadata.create_all(engine)
print("meta data after biding++++++++++",metadata.tables,engine)
print("table details +++++++++++++++++",id(metadata),id(notes))
class NoteIn(BaseModel):
    text: str
    completed: bool


class Note(BaseModel):
    id: int
    text: str
    completed: bool


app = FastAPI()


@app.on_event("startup")
async def startup():
    print("start db")
    await database.connect()


@app.on_event("shutdown")
async def shutdown():
    print("close db")
    await database.disconnect()


@app.get("/notes/", response_model=List[Note])
async def read_notes():
    print("print first")
    query = notes.select()
    return await database.fetch_all(query)


@app.post("/notes/", response_model=NoteIn)
async def create_note(note: NoteIn):
    print("first thtan post")
    with open("file.txt",mode="r") as f:
        l=f.read()
        print("read data is ",l)
    query = notes.insert().values(text=note.text, completed=note.completed)
    last_record_id = await database.execute(query)
    print("may be first than post operation")
    return {**note.dict(), "id": last_record_id}

# use of form which similar to passing argument like path,query,body
@app.post("/login")
async def login(username:str=Form(...),password:int=Form(...)):
    return {"username":username}

# USE OF FILE AND UPLOAD FILE
@app.post("/upload")
async def upload(files:bytes=File(...)):
    return {"filename":len(files)}

@app.post("/uploadfile")
async def upload(files:UploadFile=File(...)):
    return {"filename":files}


# ERROR HANDLING
items = {"id":1}
@app.get('/id_check_status/{id}')
async def id_check(id:int):
    if id not in items:
        raise HTTPException(status_code=404,detail="not found",headers={"x-error":"errors occured"})
# CUSTOM ERROR HANDLING
class Myerror(Exception):
    def __init__(self,name):
        self.name=name
@app.exception_handler(Myerror)
async def unicorn_exe_handler(request: Request,exceptions:Myerror):
    return JSONResponse(status_code=404,content={"message":f"{exceptions.name} not found"})

@app.get("/error/{name}")
async def errors(name:str):
    if name=="suraj":
        raise Myerror(name=name)
    return {"name":name}